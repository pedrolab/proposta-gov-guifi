---
modified-date: <2019-10-02 Wed 00:16>
---

# Proposta de metodologia-tecnologia per la governança dels acords a guifi

*es suggereix fer el seguiment de la proposta en https://gitlab.com/pedrolab/proposta-gov-guifi/ on hi podreu crear incidències i propostes de canvi*

- els documents s'escriuran en un fitxer de text codificació UTF-8 en format markdown que facilitarà el seu accés universal, export a qualsevol format extra amb eines com pandoc.org i un fàcil control de canvis (amb qualsevol eina que faci diferències entre fitxers, versions, software de control de versions).

    - això inclou llicència guifi, [i aquí la proposta concreta d'adaptació](https://github.com/guifi/guifi-cxoln/pull/2)

- els documents estaran en un projecte gitlab de la organització guifi tal com git.guifi.net/guifi/gov que serà el sistema de fitxers en una disposició seqüencial similar als fitxers de IETF (rfcn, exemple: https://tools.ietf.org/html/rfc1035) o RIPE (ripe-n, exemple: https://www.ripe.net/publications/docs/ripe-533), del tipus `guifi-n.md`. S'utilitzarà `guifi-n-llengua.md` en el nom del fitxer per mostrar en quina llengua està escrit. Per exemple, la llicència adaptada a format markdown podria ser `guifi-2-cat.md` i si es suggereix modificació de `guifi-2-cat`, llavors es publicaria com a `guifi-3-cat` i llavors es marcaria en desús `guifi-2`. Per altre banda i com exemple real [els RFC de rust antics han sigut modificats](https://github.com/rust-lang/rfcs/tree/master/text); poster la situació estaria al mig entre ser estricte en la publicació i admetre petites modificacions (el sistema de control de canvis ho permet)

    - formes de citar el document són: (1) `guifi-1`, (2) `guifi-1-cat`, (3) "document 1 [de guifi]"

    - un document pot tenir arrel en qualsevol dels idiomes següents: català (cat), castellà (cast) o anglès (en). Per aprovar un nou document no cal tenir el 100% de traduccions, que poden arribar després, si arriben. El document es podria veure's com `guifi-1-cat`, `guifi-1-cast` i `guifi-1-en` en el cas d'un document amb traducció 100% en els 3 idiomes. Addicionalment podria estar en altres idiomes que s'haurien de delimitar l'abreviació en el nom del document

    - el document sempre tindrà el format font en markdown i es recomana fer exports per fer-los accessibles a diferents medis (pdf, html, doc, png, jpg, etc.) però no guardar-los en el sistema de fitxers del control de canvis

    - el document inclourà metadata en format yaml [a l'estil jekyll](https://github.blog/2013-09-27-viewing-yaml-metadata-in-your-documents/). Les paraules clau o keywords només estaran en un idioma: en anglès

- es proposa aquest document com a `guifi-1-cat` (comenteu si voleu que algun document vagi abans com la llicència guifi, acords econòmics, canvis llicència, etc.)

- l'aplicació de la governança és en la branca master, les altres branques poden tenir usos diversos a criteri de la comunitat

- la pestanya Issues de gitlab és un sistema de tickets que servirà per obrir dubtes o conflictes sobre l'estat de la governança actual (que és el sistema de fitxers)

- la pestanya Merge requests és un sistema de tickets que rep propostes de canvis que aquests només poden ser de documents nous i només es poden integrar quan siguin correctes: ortografia, coherència, aprovació segons governança. Un cop s'ha fusionat el document (merge) no es pot tornar enrere. Per això és important que hi hagi un responsable-editor o grup d'editors que mantinguin el repositori des del punt de vista de "correcció"

    - exemple de correcció-comentaris d'un merge request complex (pull request a github): https://github.com/rust-lang/rfcs/pull/48

- en les diferents incidències de Issues i Merge requests es poden afegir comentaris que enriqueixen el fil de discussió per la millora o enteniment de la governança

- també hi ha la possibilitat de votar a favor o en contra de diferents Issues i Merge requests, i aquestes es poden ordenar per popularitat (sort by popularity) per entendre quins tickets tenen prioritat, hi ha una idea molt clara de si s'hauria d'aprovar o rebutjar (en els casos on hi ha molts més a favor que en contra, o tots a favor o tots en contra) i/o requereixen d'una sessió presencial per aprofundir (quan hi ha empats o propers a empat en a favor i en contra)

- es recomanable que el desenvolupament dels nous documents siguin en altres projectes gitlab utilitzant l'eina fork

- es recomanable que es desplegui un gitlab en servidor propi (debian omnibus packages funciona molt bé i es fàcil de mantenir) per tal de tenir control sobre els "Terms of Service", la propietat intel·lectual i de les dades dels participants, així com un connector LDAP d'usuaris de GLIR i/o altres canvis que es vulguin portar a terme. La situació seria pitjor hipotèticament en utilitzar github.com enlloc de gitlab.com o instàncies derivades. Nota adicional: en gitlab els usuaris poden exportar i importar els projectes (repo git + issues + merge requests)

- es recomanable redirecció de propostes.guifi.net a git.guifi.net/guifi/gov

- els fitxers podran ser consultables en *estil FTP* com per exemple en el cas del RIPE en el següent enllaç ftp://ftp.ripe.net/ripe/docs/

- hi haurà programes d'automatització i processat del sistema de fitxers de governança, els programes recomanats estaran escrits i comentats en anglès per aprofitament i cooperació internacional

    - un programa que generarà un índex https://www.rfc-editor.org/rfc-index.txt

    - un programa permetrà separar per categories (gràcies a una correcta organització dels yaml headers definits abans) https://www.ripe.net/publications/docs (que s'han extret del yaml de metadades)
